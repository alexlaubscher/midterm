// ppm_io.c
// 601.220, Fall 2018
// Alex Laubscher alaubsc1
// Katharine Lee klee223

#include <assert.h>
#include <stdlib.h>
#include "ppm_io.h"
#include <math.h>
/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp); 

  char type[2];
  int row, col, colourcode;
  fgets(type,3,fp);
  char c = fgetc(fp);
  char a = fgetc(fp);//Checking for comments
  if(a=='#'){while(a!='\n'){a=fgetc(fp);}
  }else{
    ungetc(a,fp);
    ungetc(c, fp);
  }
  
  fscanf(fp,"%d",&col);
  fscanf(fp,"%d",&row);
  fscanf(fp,"%d",&colourcode);
  
  Image *temp = (Image*)malloc(sizeof(Image));
  temp -> rows = row;
  temp -> cols = col;
  strcpy(temp -> type,type);
  temp -> colourcode = colourcode;
  int i,j;

  Pixel *rgb = (Pixel *)malloc(row*col*sizeof(Pixel));
  for (i = 0; i < row; i++) { 
      for (j = 0; j < (col); j++) {
	char space;
	fscanf(fp,"%c",&space);
	fread(rgb, 3 * row, col, fp);   
      }
  }

  temp -> data = rgb;
  
  return temp;
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  assert(fp);					  // check that fp is not NULL

  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);

  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Pixel data failed to write properly.\n");
  }

  return num_pixels_written;
}

void swap(Image *im)
{
  
  for(int i = 0;i< (im->rows)*(im->cols);i++)
  {
    unsigned char tempR,tempB,tempG;
    tempR = ((im->data)+i)->r;
    tempG = ((im->data)+i)->g;
    tempB = ((im->data)+i)->b;
    
    ((im->data)+i)->r = tempG;
    ((im->data)+i)->g = tempB;
    ((im->data)+i)->b = tempR;

  }
  
}

void grayscale(Image *im)
{
  for(int i = 0;i< (im->rows)*(im->cols);i++)
  {
    unsigned char tempR,tempB,tempG, intensity;
    tempR = ((im->data)+i)->r;
    tempG = ((im->data)+i)->g;
    tempB = ((im->data)+i)->b;

    intensity = 0.30*tempR + 0.59*tempG + 0.11*tempB;

    ((im->data)+i)->r = intensity;
    ((im->data)+i)->g = intensity;
    ((im->data)+i)->b = intensity;

  }
}                                                                                                         
unsigned char contrast_factor(unsigned char colour, double scale){
                                                                      
  double col = (((double)colour)/255)-0.5;
  col = col * scale;

  unsigned char tempcol;
  if(col>0.5)
    {tempcol = 255;}
  else if(col<-0.5)
    {tempcol = 0;}
  else
    {tempcol = (col+0.5)*255;}
  return tempcol;

}

void contrast(Image *im, double scale)
{
  unsigned char tempR,tempB,tempG;
  for(int i = 0;i< (im->rows)*(im->cols);i++)
  {
    tempR = ((im->data)+i)->r;
    tempG = ((im->data)+i)->g;
    tempB = ((im->data)+i)->b;
                                                                            
    tempR =contrast_factor(tempR,scale);
    tempG =contrast_factor(tempG,scale);
    tempB =contrast_factor(tempB,scale);  
    ((im->data)+i)->r = tempR;
    ((im->data)+i)->g = tempG;
    ((im->data)+i)->b = tempB;
  
  }
}

void zoom_in(Image *im)
{
  int rows = im->rows;
  int cols = im->cols;
 
  int temp_rows = im->rows * 2;
  int temp_cols = im->cols * 2;

  Pixel *new = (Pixel *)malloc(temp_rows*temp_cols*sizeof(Pixel));

  int j = 0;
  int k = 0;
  for (int y = 0; y < rows; y++) {
    k = 0;
    for (int x = 0; x < cols; x++) {

      unsigned char r1 = ((im->data)+(cols*y)+x)->r;
      unsigned char g1 = ((im->data)+(cols*y)+x)->g;
      unsigned char b1 = ((im->data)+(cols*y)+x)->b;

      (new+(k+(temp_cols*j)))->r = r1;
      (new+(k+(temp_cols*j)))->g = g1;
      (new+(k+(temp_cols*j)))->b = b1;
      
      (new+(k+1+(temp_cols*j)))->r = r1;
      (new+(k+1+(temp_cols*j)))->g = g1;
      (new+(k+1+(temp_cols*j)))->b = b1;

      (new+(k+((temp_cols*(j+1)))))->r = r1;
      (new+(k+((temp_cols*(j+1)))))->g = g1;
      (new+(k+((temp_cols*(j+1)))))->b = b1;

      (new+(k+1+((temp_cols*(j+1)))))->r = r1;
      (new+(k+1+((temp_cols*(j+1)))))->g = g1;
      (new+(k+1+((temp_cols*(j+1)))))->b = b1;
      k+=2;
    }
    j+=2;
  }

  Pixel *temp = im->data;
  im->data = (Pixel *)realloc(im->data, sizeof(Pixel)*temp_rows*temp_cols);
  im->data = new;
  free(temp);
  im->rows = temp_rows;
  im->cols = temp_cols;
 
}

void zoom_out(Image *im)
{
  int rows = im->rows;
  int cols = im->cols;
  
  int temp_rows = rows / 2;
  int temp_cols = cols / 2;
  
  Pixel *new = (Pixel *)malloc(temp_rows*temp_cols*sizeof(Pixel));
  int k=0;
  int j=0;
  for (int y = 0; y < rows; y+=2) {
    j = 0;
    for (int x = 0; x < cols; x += 2) {
      unsigned char r1 = ((im->data)+(cols*y)+x)->r;
      unsigned char r2 = ((im->data)+(cols*y)+x+1)->r;
      unsigned char r3 = ((im->data)+(cols*(y+1))+x)->r;
      unsigned char r4 = ((im->data)+(cols*(y+1))+x+1)->r;
      unsigned char avg_r = (r1+r2+r3+r4) / 4;
      
      unsigned char g1 = ((im->data)+(cols*y)+x)->g;
      unsigned char g2 = ((im->data)+(cols*y)+x+1)->g;
      unsigned char g3 = ((im->data)+(cols*(y+1))+x)->g;
      unsigned char g4 = ((im->data)+(cols*(y+1))+x+1)->g;
      unsigned char avg_g = (g1+g2+g3+g4) / 4;

      unsigned char b1 = ((im->data)+(cols*y)+x)->b;
      unsigned char b2 = ((im->data)+(cols*y)+x+1)->b;
      unsigned char b3 = ((im->data)+(cols*(y+1))+x)->b;
      unsigned char b4 = ((im->data)+(cols*(y+1))+x+1)->b;
      unsigned char avg_b = (b1+b2+b3+b4) / 4;

      (new+(k*temp_cols+j))->r = avg_r;
      (new+(k*temp_cols+j))->g = avg_g;
      (new+(k*temp_cols+j))->b = avg_b;
      j++;
    }
    k++;
  }

  Pixel *temp = im->data;
  im->data = new;
  free(temp);
  im->rows = temp_rows;
  im->cols = temp_cols;
}

void occlude(Image *im, int x1, int y1, int x2, int y2)
{
  int length = im->cols;
  
  for (int i = x1; i < x2; i++) {
    for (int k = y1; k < y2; k++) {
      ((im->data)+(length*k)+i)->r = 0;
      ((im->data)+(length*k)+i)->g = 0;
      ((im->data)+(length*k)+i)->b = 0;
    }
  }
}

double sq(double num) {
  return num * num;
}

void gen_matrix(float sigma, int size, double **matrix) {
  int center = size / 2;

  for (int i = 0; i < size; i++) {
    for (int k = 0; k < size; k++) {
      int dx = center - i;
      int dy = center - k;
      matrix[i][k] = (1.0 / (2.0 * 3.14159 * sq(sigma))) * exp(-(sq(dx) + sq(dy)) / (2 * sq(sigma)));
    }
  }

}

void one_pixel(int x, int y, double **matrix, Image *im, int size) {
  
  int cols = im->cols;
  
  double pixels_c = 0;
  double r_sum = 0;
  double g_sum = 0;
  double b_sum = 0;
  double avg = 0;
  int a = 0;
  int b = 0;

  int offset = (y * cols) + x + 1;

  for (int i = (size / 2) * -1; i < size / 2 + 1; i++) {
    b = 0;
    for (int k = (size / 2) * -1; k < size / 2 + 1; k++) {
      if (x+k > -1 && y+i > -1 && x+k < im->cols + 1 && y+i < im->rows + 1) {
	double factor = matrix[a][b];
	r_sum += ((im->data)+offset+(k)+(i*cols))->r * factor;
	g_sum += ((im->data)+offset+(k)+(i*cols))->g * factor;
	b_sum += ((im->data)+offset+(k)+(i*cols))->b * factor;
	avg += matrix[a][b];
	pixels_c++;
      }
      b++;
    }
    a++;
  }

  printf("%f %f\n", r_sum, avg);
  r_sum = r_sum / avg;
  g_sum = g_sum / avg;
  b_sum = b_sum / avg;

  ((im->data)+offset)->r = r_sum;
  ((im->data)+offset)->g = g_sum;
  ((im->data)+offset)->b = b_sum;
}

void blur(Image *im, float sigma)
{
  int size = sigma * 10;
  if (size % 2 == 0) {
    size++;
  }
  
  double **matrix = malloc(sizeof(double*) * size);
  for (int i = 0; i < size; i++) {
    matrix[i] = malloc(sizeof(double) * size);
  }
  
  int rows = im->rows;
  int cols = im->cols;

  gen_matrix(sigma, size, matrix);
  
  for (int i = 0; i < rows; i++) {
    for (int k = 0; k < cols; k++) {
      one_pixel(k, i, matrix, im, size);
    }
  }
  
  for (int i = 0; i < size; i++) {
    free(matrix[i]);
  }
  free(matrix);
}

