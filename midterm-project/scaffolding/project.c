//Midterm Project
//Katharine Lee klee223
//Alex Laubscher alaubsc1
//October 24 2018

#include <assert.h>
#include <stdlib.h>
#include "ppm_io.h"
#include <stdio.h>
int main(int argc, char*argv[]) {
  						//READING IN PPM
  if(argc<3){	 				//check for minimum no. arguments
    fprintf(stderr,"Missing input or output files or both.\n");
    return 1;					//exit 1
    }
  
  FILE *fp = fopen(argv[1],"rb");
  if(!fp){					//Check if input file was opened
      fprintf(stderr,"Input file could not be opened.\n");
      return 2;					//exit 2
    }
     
  Image * im = read_ppm(fp);			//Read ppm file into Image struct

  if(!im){					//Check if Image struct was created
    fprintf(stderr,"Could not read image file.\n");	
    return 3;					//exit 3
   }

  if(strcmp(im->type,"P6")||im->colourcode!=255){//Check if properly formatted ppm
      fprintf(stderr,"Improperly formatted ppm file.\n");
      return 3;					//exit 3
    }

  
  int op;
  if(strcmp(argv[3],"swap")==0){op = 1;}	//Setting up switch numbers for each
  if(strcmp(argv[3],"grayscale")==0){op = 2;}	//operation.
  if(strcmp(argv[3],"contrast")==0){op = 3;}
  if(strcmp(argv[3],"zoom_in")==0){op = 4;}
  if(strcmp(argv[3],"zoom_out")==0){op = 5;}
  if(strcmp(argv[3],"occlude")==0){op = 6;}
  if(strcmp(argv[3],"blur")==0){op = 7;}

  int arguments = argc - 4; 			//Find number of args after operation
  switch(op){
  //SWAP
    case 1: 					
    {
      if(arguments!=0)				//check number of arguments
	{fprintf(stderr,"Incorrect number of arguments.\n");
	 return 5;}
      swap(im);					//swap image
      break;
    }
 //GRAYSCALE
    case 2: 					
    {
      if(arguments!=0)				//check number of arguments
        {fprintf(stderr,"Incorrect number of arguments.\n");
	 return 5;}
      grayscale(im);				//grayscale image
      break;
    }
 //CONTRAST
    case 3: 					
    {
      if(arguments!=1)				//check number of arguments
	{fprintf(stderr,"Incorrect number of arguments.\n");
	 return 5;}
      double d = atof(argv[4]);			//convert scale from str to dbl
      contrast(im,d);				//contrast image
      break;
    }
 //ZOOM_IN
    case 4: 					
    {
      if(arguments!=0)			//check number of arguments
      {fprintf(stderr,"Incorrect number of arguments.\n");
      return 5;}
      zoom_in(im);
      break;
    }
 //ZOOM_OUT
    case 5: 	
    {
      if(arguments!=0)			//check number of arguments
	{fprintf(stderr,"Incorrect number of arguments.\n");
	  return 5;}
      zoom_out(im);
      break;
    }
 //OCCLUDE   
    case 6:
    { 					
      if(arguments!=4)				//check number of arguments
        {fprintf(stderr,"Incorrect number of arguments.\n");
        return 5;}
      
      int x1 = atoi(argv[4]);
      int y1 = atoi(argv[5]);
      int x2 = atoi(argv[6]);
      int y2 = atoi(argv[7]);

      if(x1>x2 || y1>y2 || x1<-1||x2>im->cols||y1<-1||y2>im->rows)
	{fprintf(stderr,"Arguments for occlude operation were out of range or invalid\n");
	  return 6;}
	 
      occlude(im, x1, y1, x2, y2);
      break;
    }
 //BLUR
    case 7:
    {					
      if(arguments!=1)			//check number of arguments
	{fprintf(stderr,"Incorrect number of arguments.\n");
	  return 5;}
      float sigma = atof(argv[4]);
      blur(im, sigma);
      
      break;
    }
    default:
      {
        fprintf(stderr,"No operation name was specified, or operation name specified was invalid.\n");
	return 4;				//exit 4
	break;
      }
  }
  
  FILE *output = fopen("output.ppm","wb");	//WRITING PPM
  if(!output){					//Check if output file was opened
      fprintf(stderr,"Output file could not be opened.\n");
      return 7;					//exit 7
    }
  
  write_ppm(output, im);			//Write ppm file from Image struct
  
  fseek (output, 0, SEEK_END);			//Check if file is empty even
  int size = ftell(fp);				//after writing to it
  if (0 == size) {				
    fprintf(stderr,"Writing output failed.\n");		
    return 7;					//exit 7	
   }
  
  fclose(fp);					//CLOSING FILES
  fclose(output);
  free(im);
}
