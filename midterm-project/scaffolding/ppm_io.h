// ppm_io.h
// 601.220, Fall 2018
// Alex Laubscher alaubsc1
// Katharine Lee klee223


#ifndef PPM_IO_H
#define PPM_IO_H

#define PI 3.1415926


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
/* A struct to store a single RGB pixel, one byte per color channel.
 */
typedef struct _pixel {
  unsigned char r;  //red   channel value
  unsigned char g;  //green channel value
  unsigned char b;  //blue  channel value
} Pixel;


/* A struct to bundle together a pixel array with the other
 * image data we'll frequently want to pass around with it.
 * (This saves us from having to pass the same three 
 * variables to every function.) Note that no Pixels are
 * stored within this struct; the data field is a pointer.
 */
typedef struct _image {
  Pixel *data;  //pointer to array of Pixels
  int rows;     //number of rows of Pixels
  int cols;     //number of columns of Pixels
  int colourcode; //i added these two 
  char type[2];
} Image;




/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp);


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im);


void swap(Image *im);
void grayscale(Image *im);
void contrast(Image *im, double scale);
void zoom_in(Image *im);
void zoom_out(Image *im);
void occlude(Image *im, int x1, int y1, int x2, int y2);
void blur(Image *im, float sigma);

void gen_matrix(float sigma, int size, double **matrix);
void one_pixel(int x, int y, double **matrix, Image *im, int size);
double sq(double num);

unsigned char contrast_factor(unsigned char colour, double scale);
#endif
